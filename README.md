# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Feature Tracker
* 1.0

### TODO ###

* Add comments to features view.
* Add ability to upload files.
* Hide deleted entities from the users (create views?)
* Improve user interface.
* Add Authentification.
* Improve Entity ID Mapping? Set out segments?

## Notes ##

* Don't try to switch between views on the "one" page.
* Look at how bitbucket does it...  Build 'Sub layout' views... and each subpage goes from there. :)
* Definitely look around at other websites to see how their sites fit together and request new pages.