﻿using Microsoft.EntityFrameworkCore;

namespace FeatureTracker.Models
{
    public class FeatureTrackerContext : DbContext
    {
        public FeatureTrackerContext (DbContextOptions<FeatureTrackerContext> options)
            : base(options)
        {
        }

        public DbSet<Feature> Feature { get; set; }
        public DbSet<FeatureCategory> FeatureCategory { get; set; }
        public DbSet<FeatureStatus> FeatureStatus { get; set; }
        public DbSet<FeatureComment> FeatureComment { get; set; }

        public DbSet<FeatureTrackerUser> FeatureTrackerUser { get; set; }

        public DbSet<UpdateLog> UpdateLog { get; set; }
        public DbSet<UpdateLogAction> UpdateLogAction { get; set; }
    }
}
