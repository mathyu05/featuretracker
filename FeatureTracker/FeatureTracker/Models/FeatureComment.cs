﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FeatureTracker.Models
{
    public class FeatureComment
    {
        [Key]
        public int FeatureCommentId { get; set; }

        [Required]
        public int FeatureId { get; set; }

        [Required]
        public int CommentingUserId { get; set; }
        [ForeignKey("CommentingUserId")]
        public virtual FeatureTrackerUser CommentingUser { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime CreationDate { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime UpdatedDate { get; set; }

        [Required]
        public string Comment { get; set; }
    }
}
