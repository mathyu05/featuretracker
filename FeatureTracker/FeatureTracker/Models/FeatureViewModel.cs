﻿using System.Collections.Generic;

namespace FeatureTracker.Models
{
    public class FeatureViewModel
    {
        public FeatureViewModel(Feature feature, List<FeatureComment> featureComment)
        {
            Feature = feature;
            FeatureComments = featureComment;
        }

        public Feature Feature { get; set; }

        public List<FeatureComment> FeatureComments { get; set; }
    }
}
