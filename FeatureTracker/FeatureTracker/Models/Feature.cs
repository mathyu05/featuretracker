﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FeatureTracker.Models
{
    public class Feature
    {
        [Key]
        [Display(Name = "Feature ID")]
        public int FeatureId { get; set; }

        [Required]
        public int ReporterUserId { get; set; }
        [ForeignKey("ReporterUserId")]
        [Display(Name = "Reporter")]
        public virtual FeatureTrackerUser ReporterUser { get; set; }

        public int? AssigneeUserId { get; set; }
        [ForeignKey("AssigneeUserId")]
        [Display(Name = "Assignee")]
        public virtual FeatureTrackerUser AssigneeUser { get; set; }

        [Required]
        public string Title { get; set; }
        public string Description { get; set; }

        [Required]
        public int FeatureCategoryId { get; set; }
        [ForeignKey("FeatureCategoryId")]
        [Display(Name = "Feature Category")]
        public virtual FeatureCategory FeatureCategory { get; set; }

        [Required]
        public int FeatureStatusId { get; set; }
        [ForeignKey("FeatureStatusId")]
        [Display(Name = "Feature Status")]
        public virtual FeatureStatus FeatureStatus { get; set; }

        [Required]
        [Display(Name = "Creation")]
        [DataType(DataType.Date)]
        public DateTime CreationDate { get; set; }
        [Required]
        [Display(Name = "Updated")]
        [DataType(DataType.Date)]
        public DateTime UpdatedDate { get; set; }

        public int? LastUpdateUserId { get; set; }
        [ForeignKey("LastUpdateUserId")]
        public virtual FeatureTrackerUser LastUpdateUser { get; set; }

        [Required]
        public bool Deleted { get; set; }
    }
}
