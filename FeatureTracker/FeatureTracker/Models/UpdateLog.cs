﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FeatureTracker.Models
{
    public class UpdateLog
    {
        [Key]
        public int UpdateId { get; set; }

        public int? UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual FeatureTrackerUser UpdatingUser { get; set; }

        public int ActionId { get; set; }
        [ForeignKey("ActionId")]
        public virtual UpdateLogAction Action { get; set; }

        public DateTime DateUpdated { get; set; }

        public string TableName { get; set; }

        public int ObjectId { get; set; }
        public string FieldName { get; set; }

        public string OldValue { get; set; }
        public string NewValue { get; set; }
    }
}
