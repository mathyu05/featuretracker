﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FeatureTracker.Models
{
    public class FeatureArtefact
    {
        [Key]
        public int FeatureArtefactId { get; set; }

        [Required]
        public int FeatureId { get; set; }
        [ForeignKey("FeatureId")]
        public virtual Feature Feature { get; set; }

        [Required]
        public byte[] Artefact { get; set; }

        [Required]
        [Display(Name = "Creation")]
        [DataType(DataType.Date)]
        public DateTime CreationDate { get; set; }

        [Required]
        public bool Deleted { get; set; }
    }
}
