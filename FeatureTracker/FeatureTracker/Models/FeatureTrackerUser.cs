﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FeatureTracker.Models
{
    public class FeatureTrackerUser
    {
        [Key]
        public int FeatureTrackerUserId { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        public string Email { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime CreationDate { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime UpdatedDate { get; set; }

        public int? LastUpdateUserId { get; set; }
        [ForeignKey("LastUpdateUserId")]
        public virtual FeatureTrackerUser LastUpdateUser { get; set; }

        [Required]
        public bool Deleted { get; set; }
    }
}
