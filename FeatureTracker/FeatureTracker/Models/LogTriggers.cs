﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FeatureTracker.Models
{
    public static class LogTriggers
    {
        public static void AttachLogTriggers(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(CreateFeatureTrigger());
            migrationBuilder.Sql(CreateFeatureCategoryTrigger());
            migrationBuilder.Sql(CreateFeatureStatusTrigger());
            migrationBuilder.Sql(CreateFeatureCommentTrigger());
            migrationBuilder.Sql(CreateFeatureTrackerUserTrigger());
        }

        private static string CreateFeatureTrigger()
        {
            string tableName = "Feature";
            string primaryKeyColumn = "FeatureId";
            string UserUpdatingColumn = "LastUpdateUserId";

            string triggerDeclarations = string.Format(@"CREATE TRIGGER featureTriggerAfterInsertOrUpdate
                                                        ON {0}
                                                        FOR
                                                        INSERT, UPDATE
                                                        AS"
                                            , tableName);

            return string.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}{0}{8}{0}{9}"
                        , Environment.NewLine
                        , triggerDeclarations
                        , CreateSubTrigger(tableName, "AssigneeUserId", primaryKeyColumn, UserUpdatingColumn, 2, 1)
                        , CreateSubTrigger(tableName, "ReporterUserId", primaryKeyColumn, UserUpdatingColumn, 2, 1)
                        , CreateSubTrigger(tableName, "Title", primaryKeyColumn, UserUpdatingColumn, 2, 1)
                        , CreateSubTrigger(tableName, "Description", primaryKeyColumn, UserUpdatingColumn, 2, 1)
                        , CreateSubTrigger(tableName, "FeatureCategoryId", primaryKeyColumn, UserUpdatingColumn, 2, 1)
                        , CreateSubTrigger(tableName, "FeatureStatusId", primaryKeyColumn, UserUpdatingColumn, 2, 1)
                        , CreateSubTrigger(tableName, "CreationDate", primaryKeyColumn, UserUpdatingColumn, 2, 1)
                        , CreateSubTrigger(tableName, "Deleted", primaryKeyColumn, UserUpdatingColumn, 3, 1));
        }

        private static string CreateFeatureCategoryTrigger()
        {
            string tableName = "FeatureCategory";
            string primaryKeyColumn = "FeatureCategoryId";
            string UserUpdatingColumn = "LastUpdateUserId";

            string triggerDeclarations = string.Format(@"CREATE TRIGGER featureCategoryTriggerAfterInsertOrUpdate
                                                        ON {0}
                                                        FOR
                                                        INSERT, UPDATE
                                                        AS"
                                            , tableName);

            return string.Format("{1}{0}{2}{0}{3}"
                        , Environment.NewLine
                        , triggerDeclarations
                        , CreateSubTrigger(tableName, "Name", primaryKeyColumn, UserUpdatingColumn, 2, 1)
                        , CreateSubTrigger(tableName, "Deleted", primaryKeyColumn, UserUpdatingColumn, 3, 1));
        }

        private static string CreateFeatureStatusTrigger()
        {
            string tableName = "FeatureStatus";
            string primaryKeyColumn = "FeatureStatusId";
            string UserUpdatingColumn = "LastUpdateUserId";

            string triggerDeclarations = string.Format(@"CREATE TRIGGER featureStatusTriggerAfterInsertOrUpdate
                                                        ON {0}
                                                        FOR
                                                        INSERT, UPDATE
                                                        AS"
                                            , tableName);

            return string.Format("{1}{0}{2}{0}{3}"
                        , Environment.NewLine
                        , triggerDeclarations
                        , CreateSubTrigger(tableName, "Name", primaryKeyColumn, UserUpdatingColumn, 2, 1)
                        , CreateSubTrigger(tableName, "Deleted", primaryKeyColumn, UserUpdatingColumn, 2, 1));
        }

        private static string CreateFeatureCommentTrigger()
        {
            string tableName = "FeatureComment";
            string primaryKeyColumn = "FeatureCommentId";
            string UserUpdatingColumn = "CommentingUserId";

            string triggerDeclarations = string.Format(@"CREATE TRIGGER featureCommentTriggerAfterInsertOrUpdate
                                                        ON {0}
                                                        FOR
                                                        INSERT, UPDATE
                                                        AS"
                                            , tableName);

            return string.Format("{1}{0}{2}"
                        , Environment.NewLine
                        , triggerDeclarations
                        , CreateSubTrigger(tableName, "Comment", primaryKeyColumn, UserUpdatingColumn, 2, 1));
        }

        private static string CreateFeatureTrackerUserTrigger()
        {
            string tableName = "FeatureTrackerUser";
            string primaryKeyColumn = "FeatureTrackerUserId";
            string UserUpdatingColumn = "LastUpdateUserId";

            string triggerDeclarations = string.Format(@"CREATE TRIGGER featureTrackerUserTriggerAfterInsertOrUpdate
                                                        ON {0}
                                                        FOR
                                                        INSERT, UPDATE
                                                        AS"
                                            , tableName);

            return string.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}"
                        , Environment.NewLine
                        , triggerDeclarations
                        , CreateSubTrigger(tableName, "FirstName", primaryKeyColumn, UserUpdatingColumn, 2, 1)
                        , CreateSubTrigger(tableName, "LastName", primaryKeyColumn, UserUpdatingColumn, 2, 1)
                        , CreateSubTrigger(tableName, "Email", primaryKeyColumn, UserUpdatingColumn, 2, 1)
                        , CreateSubTrigger(tableName, "Deleted", primaryKeyColumn, UserUpdatingColumn, 3, 1));
        }

        private static string CreateSubTrigger(string tableName, string fieldName, string primaryKeyColumn, string UserUpdatingColumn, int firstActionId, int secondActionId)
        {
            return string.Format(@"IF EXISTS (SELECT * FROM DELETED)
                            BEGIN
                                INSERT INTO UpdateLog(UserId, ActionId, DateUpdated, TableName, ObjectId, FieldName, OldValue, NewValue)
                                    SELECT i.{5}
                                            , {2}
                                            , GETDATE()
                                            , '{0}'
                                            , i.{4}
                                            , '{1}'
                                            , d.{1}
                                            , i.{1}
                                    FROM INSERTED i
                                    INNER JOIN DELETED d
                                        ON i.{4} = d.{4}
                                    WHERE i.{1} <> d.{1}
                            END
                            ELSE
                            BEGIN
                                INSERT INTO UpdateLog(UserId, ActionId, DateUpdated, TableName, ObjectId, FieldName, OldValue, NewValue)
                                    SELECT i.{5}
                                            , {3}
                                            , GETDATE()
                                            , '{0}'
                                            , i.{4}
                                            , '{1}'
                                            , NULL
                                            , i.{1}
                                    FROM INSERTED i
                            END"
                , tableName, fieldName, firstActionId, secondActionId, primaryKeyColumn, UserUpdatingColumn);
        }
    }
}
