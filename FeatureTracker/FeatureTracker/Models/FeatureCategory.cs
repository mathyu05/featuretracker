﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FeatureTracker.Models
{
    public class FeatureCategory
    {
        [Key]
        public int FeatureCategoryId { get; set; }
        [Required]
        public string Name { get; set; }

        public int? LastUpdateUserId { get; set; }
        [ForeignKey("LastUpdateUserId")]
        public virtual FeatureTrackerUser LastUpdateUser { get; set; }

        [Required]
        public bool Deleted { get; set; }
    }
}
