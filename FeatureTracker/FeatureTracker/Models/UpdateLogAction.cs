﻿using System.ComponentModel.DataAnnotations;

namespace FeatureTracker.Models
{
    public class UpdateLogAction
    {
        [Key]
        public int UpdateLogActionId { get; set; }
        [Required]
        public string Name { get; set; }

        [Required]
        public bool Deleted { get; set; }
    }
}
