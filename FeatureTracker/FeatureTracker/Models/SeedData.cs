﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FeatureTracker.Models
{
    public static class SeedData
    {
        public static void Initialize(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(Seed("UpdateLogAction", "(Name, Deleted)", "('INSERT', 0), ('UPDATE', 0), ('DELETE', 0)"));
            migrationBuilder.Sql(Seed("FeatureTrackerUser", "(FirstName, LastName, CreationDate, UpdatedDate, Deleted)", "('System', 'Administrator', GETDATE(), GETDATE(), 0)"));
            migrationBuilder.Sql(Seed("FeatureCategory", "(Name, LastUpdateUserId, Deleted)", "('Feature', 1, 0), ('Defect', 1, 0), ('Investigation', 1, 0), ('Question', 1, 0)"));
            migrationBuilder.Sql(Seed("FeatureStatus", "(Name, LastUpdateUserId, Deleted)", "('Pending', 1, 0), ('Development', 1, 0), ('Testing', 1, 0), ('Complete', 1, 0)"));
        }

        private static string Seed(string tableName, string columns, string values)
        {
            return string.Format(@"IF NOT EXISTS (SELECT * FROM {0})
                            BEGIN
                                INSERT INTO {0} {1}
                                VALUES {2}
                            END"
                , tableName, columns, values);
        }
    }
}
