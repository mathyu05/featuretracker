﻿using System;
using FeatureTracker.Models;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FeatureTracker.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FeatureTrackerUser",
                columns: table => new
                {
                    FeatureTrackerUserId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    LastUpdateUserId = table.Column<int>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeatureTrackerUser", x => x.FeatureTrackerUserId);
                    table.ForeignKey(
                        name: "FK_FeatureTrackerUser_FeatureTrackerUser_LastUpdateUserId",
                        column: x => x.LastUpdateUserId,
                        principalTable: "FeatureTrackerUser",
                        principalColumn: "FeatureTrackerUserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UpdateLogAction",
                columns: table => new
                {
                    UpdateLogActionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpdateLogAction", x => x.UpdateLogActionId);
                });

            migrationBuilder.CreateTable(
                name: "FeatureCategory",
                columns: table => new
                {
                    FeatureCategoryId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    LastUpdateUserId = table.Column<int>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeatureCategory", x => x.FeatureCategoryId);
                    table.ForeignKey(
                        name: "FK_FeatureCategory_FeatureTrackerUser_LastUpdateUserId",
                        column: x => x.LastUpdateUserId,
                        principalTable: "FeatureTrackerUser",
                        principalColumn: "FeatureTrackerUserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FeatureComment",
                columns: table => new
                {
                    FeatureCommentId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FeatureId = table.Column<int>(nullable: false),
                    CommentingUserId = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    Comment = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeatureComment", x => x.FeatureCommentId);
                    table.ForeignKey(
                        name: "FK_FeatureComment_FeatureTrackerUser_CommentingUserId",
                        column: x => x.CommentingUserId,
                        principalTable: "FeatureTrackerUser",
                        principalColumn: "FeatureTrackerUserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FeatureStatus",
                columns: table => new
                {
                    FeatureStatusId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    LastUpdateUserId = table.Column<int>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeatureStatus", x => x.FeatureStatusId);
                    table.ForeignKey(
                        name: "FK_FeatureStatus_FeatureTrackerUser_LastUpdateUserId",
                        column: x => x.LastUpdateUserId,
                        principalTable: "FeatureTrackerUser",
                        principalColumn: "FeatureTrackerUserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UpdateLog",
                columns: table => new
                {
                    UpdateId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: true),
                    ActionId = table.Column<int>(nullable: false),
                    DateUpdated = table.Column<DateTime>(nullable: false),
                    TableName = table.Column<string>(nullable: true),
                    ObjectId = table.Column<int>(nullable: false),
                    FieldName = table.Column<string>(nullable: true),
                    OldValue = table.Column<string>(nullable: true),
                    NewValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpdateLog", x => x.UpdateId);
                    table.ForeignKey(
                        name: "FK_UpdateLog_UpdateLogAction_ActionId",
                        column: x => x.ActionId,
                        principalTable: "UpdateLogAction",
                        principalColumn: "UpdateLogActionId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UpdateLog_FeatureTrackerUser_UserId",
                        column: x => x.UserId,
                        principalTable: "FeatureTrackerUser",
                        principalColumn: "FeatureTrackerUserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Feature",
                columns: table => new
                {
                    FeatureId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ReporterUserId = table.Column<int>(nullable: false),
                    AssigneeUserId = table.Column<int>(nullable: true),
                    Title = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    FeatureCategoryId = table.Column<int>(nullable: false),
                    FeatureStatusId = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    LastUpdateUserId = table.Column<int>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Feature", x => x.FeatureId);
                    table.ForeignKey(
                        name: "FK_Feature_FeatureTrackerUser_AssigneeUserId",
                        column: x => x.AssigneeUserId,
                        principalTable: "FeatureTrackerUser",
                        principalColumn: "FeatureTrackerUserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Feature_FeatureCategory_FeatureCategoryId",
                        column: x => x.FeatureCategoryId,
                        principalTable: "FeatureCategory",
                        principalColumn: "FeatureCategoryId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Feature_FeatureStatus_FeatureStatusId",
                        column: x => x.FeatureStatusId,
                        principalTable: "FeatureStatus",
                        principalColumn: "FeatureStatusId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Feature_FeatureTrackerUser_LastUpdateUserId",
                        column: x => x.LastUpdateUserId,
                        principalTable: "FeatureTrackerUser",
                        principalColumn: "FeatureTrackerUserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Feature_FeatureTrackerUser_ReporterUserId",
                        column: x => x.ReporterUserId,
                        principalTable: "FeatureTrackerUser",
                        principalColumn: "FeatureTrackerUserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Feature_AssigneeUserId",
                table: "Feature",
                column: "AssigneeUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Feature_FeatureCategoryId",
                table: "Feature",
                column: "FeatureCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Feature_FeatureStatusId",
                table: "Feature",
                column: "FeatureStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Feature_LastUpdateUserId",
                table: "Feature",
                column: "LastUpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Feature_ReporterUserId",
                table: "Feature",
                column: "ReporterUserId");

            migrationBuilder.CreateIndex(
                name: "IX_FeatureCategory_LastUpdateUserId",
                table: "FeatureCategory",
                column: "LastUpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_FeatureComment_CommentingUserId",
                table: "FeatureComment",
                column: "CommentingUserId");

            migrationBuilder.CreateIndex(
                name: "IX_FeatureStatus_LastUpdateUserId",
                table: "FeatureStatus",
                column: "LastUpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_FeatureTrackerUser_LastUpdateUserId",
                table: "FeatureTrackerUser",
                column: "LastUpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_UpdateLog_ActionId",
                table: "UpdateLog",
                column: "ActionId");

            migrationBuilder.CreateIndex(
                name: "IX_UpdateLog_UserId",
                table: "UpdateLog",
                column: "UserId");

            SeedData.Initialize(migrationBuilder);

            LogTriggers.AttachLogTriggers(migrationBuilder);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Feature");

            migrationBuilder.DropTable(
                name: "FeatureComment");

            migrationBuilder.DropTable(
                name: "UpdateLog");

            migrationBuilder.DropTable(
                name: "FeatureCategory");

            migrationBuilder.DropTable(
                name: "FeatureStatus");

            migrationBuilder.DropTable(
                name: "UpdateLogAction");

            migrationBuilder.DropTable(
                name: "FeatureTrackerUser");
        }
    }
}
