﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FeatureTracker.Models;

namespace FeatureTracker.Controllers
{
    public class FeatureStatusController : Controller
    {
        private readonly FeatureTrackerContext _context;

        public FeatureStatusController(FeatureTrackerContext context)
        {
            _context = context;
        }

        // GET: Configuration/FeatureStatus
        [HttpGet("Configuration/FeatureStatus")]
        public async Task<IActionResult> Index()
        {
            var featureTrackerContext = _context.FeatureStatus
                                            .Include(f => f.LastUpdateUser)
                                            .Where(f => f.Deleted != true);

            return View(await featureTrackerContext.ToListAsync());
        }

        // GET: Configuration/FeatureStatus/Create
        [HttpGet("Configuration/FeatureStatus/Create")]
        public IActionResult Create()
        {
            ViewData["LastUpdateUserId"] = new SelectList(_context.Set<FeatureTrackerUser>(), "FeatureTrackerUserId", "FirstName");
            return View();
        }

        // POST: Configuration/FeatureStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost("Configuration/FeatureStatus/Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FeatureStatusId,Name,LastUpdateUserId,Deleted")] FeatureStatus featureStatus)
        {
            if (ModelState.IsValid)
            {
                featureStatus.LastUpdateUserId = 1; // TODO change to current user
                featureStatus.Deleted = false;

                _context.Add(featureStatus);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["LastUpdateUserId"] = new SelectList(_context.Set<FeatureTrackerUser>(), "FeatureTrackerUserId", "FirstName", featureStatus.LastUpdateUserId);
            return View(featureStatus);
        }

        // GET: Configuration/FeatureStatus/Edit/5
        [HttpGet("Configuration/FeatureStatus/Edit/{id?}")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var featureStatus = await _context.FeatureStatus.FindAsync(id);
            if (featureStatus == null)
            {
                return NotFound();
            }
            ViewData["LastUpdateUserId"] = new SelectList(_context.Set<FeatureTrackerUser>(), "FeatureTrackerUserId", "FirstName", featureStatus.LastUpdateUserId);
            return View(featureStatus);
        }

        // POST: Configuration/FeatureStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost("Configuration/FeatureStatus/Edit/{id?}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("FeatureStatusId,Name,LastUpdateUserId,Deleted")] FeatureStatus featureStatus)
        {
            return await EditUtility(id, featureStatus, false);
        }

        // GET: Configuration/FeatureStatus/Delete/5
        [HttpGet("Configuration/FeatureStatus/Delete/{id?}")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var featureStatus = await _context.FeatureStatus
                .Include(f => f.LastUpdateUser)
                .FirstOrDefaultAsync(m => m.FeatureStatusId == id);
            if (featureStatus == null)
            {
                return NotFound();
            }

            return View(featureStatus);
        }

        // POST: Configuration/FeatureStatus/Delete/5
        [HttpPost("Configuration/FeatureStatus/Delete/{id?}"), ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id, [Bind("FeatureStatusId,Name,LastUpdateUserId,Deleted")] FeatureStatus featureStatus)
        {
            return await EditUtility(id, featureStatus, true);
        }

        private bool FeatureStatusExists(int id)
        {
            return _context.FeatureStatus.Any(e => e.FeatureStatusId == id);
        }

        private async Task<IActionResult> EditUtility(int id, [Bind("FeatureStatusId,Name,LastUpdateUserId,Deleted")] FeatureStatus featureStatus, bool deleting)
        {
            if (id != featureStatus.FeatureStatusId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (deleting)
                        featureStatus.Deleted = true;

                    featureStatus.LastUpdateUserId = 1; // TODO change to current user;

                    _context.Update(featureStatus);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FeatureStatusExists(featureStatus.FeatureStatusId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["LastUpdateUserId"] = new SelectList(_context.Set<FeatureTrackerUser>(), "FeatureTrackerUserId", "FirstName", featureStatus.LastUpdateUserId);
            return View(featureStatus);
        }

    }
}
