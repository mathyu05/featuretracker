﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FeatureTracker.Models;
using System;

namespace FeatureTracker.Controllers
{
    public class FeaturesController : Controller
    {
        private readonly FeatureTrackerContext _context;

        public FeaturesController(FeatureTrackerContext context)
        {
            _context = context;
        }

        // GET: Features
        public async Task<IActionResult> Index()
        {
            var featureTrackerContext = _context.Feature
                                            .Include(f => f.AssigneeUser)
                                            .Include(f => f.FeatureCategory)
                                            .Include(f => f.FeatureStatus)
                                            .Include(f => f.LastUpdateUser)
                                            .Include(f => f.ReporterUser)
                                            .Where(f => f.Deleted != true);

            return View(await featureTrackerContext.ToListAsync());
        }

        // GET: Features/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var feature = await _context.Feature
                .Include(f => f.AssigneeUser)
                .Include(f => f.FeatureCategory)
                .Include(f => f.FeatureStatus)
                .Include(f => f.LastUpdateUser)
                .Include(f => f.ReporterUser)
                .FirstOrDefaultAsync(m => m.FeatureId == id);

            var featureComment = await _context.FeatureComment
                .Where(fc => fc.FeatureId == id)
                .OrderBy(fc => fc.CreationDate)
                .ToListAsync();

            if (feature == null || featureComment == null)
            {
                return NotFound();
            }
            
            return View(new FeatureViewModel(feature, featureComment));
        }

        // GET: Features/Create
        public IActionResult Create()
        {
            ViewData["AssigneeUserId"] = new SelectList(_context.Set<FeatureTrackerUser>(), "FeatureTrackerUserId", "FirstName");
            ViewData["FeatureCategoryId"] = new SelectList(_context.Set<FeatureCategory>(), "FeatureCategoryId", "Name");
            ViewData["FeatureStatusId"] = new SelectList(_context.Set<FeatureStatus>(), "FeatureStatusId", "Name");
            ViewData["LastUpdateUserId"] = new SelectList(_context.Set<FeatureTrackerUser>(), "FeatureTrackerUserId", "FirstName");
            ViewData["ReporterUserId"] = new SelectList(_context.Set<FeatureTrackerUser>(), "FeatureTrackerUserId", "FirstName");

            return View();
        }

        // POST: Features/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FeatureId,ReporterUserId,AssigneeUserId,Title,Description,FeatureCategoryId,FeatureStatusId,CreationDate,UpdatedDate,LastUpdateUserId,Deleted")] Feature feature)
        {
            if (ModelState.IsValid)
            {
                feature.ReporterUserId = 1; // TODO set as current user
                feature.CreationDate = DateTime.Now;
                feature.UpdatedDate = DateTime.Now;
                feature.LastUpdateUserId = 1; // TODO set as current user
                feature.Deleted = false;

                _context.Add(feature);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            ViewData["AssigneeUserId"] = new SelectList(_context.Set<FeatureTrackerUser>(), "FeatureTrackerUserId", "FirstName", feature.AssigneeUserId);
            ViewData["FeatureCategoryId"] = new SelectList(_context.Set<FeatureCategory>(), "FeatureCategoryId", "Name", feature.FeatureCategoryId);
            ViewData["FeatureStatusId"] = new SelectList(_context.Set<FeatureStatus>(), "FeatureStatusId", "Name", feature.FeatureStatusId);
            ViewData["LastUpdateUserId"] = new SelectList(_context.Set<FeatureTrackerUser>(), "FeatureTrackerUserId", "FirstName", feature.LastUpdateUserId);
            ViewData["ReporterUserId"] = new SelectList(_context.Set<FeatureTrackerUser>(), "FeatureTrackerUserId", "FirstName", feature.ReporterUserId);

            return View(feature);
        }

        // GET: Features/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var feature = await _context.Feature.FindAsync(id);

            if (feature == null)
            {
                return NotFound();
            }

            ViewData["AssigneeUserId"] = new SelectList(_context.Set<FeatureTrackerUser>(), "FeatureTrackerUserId", "FirstName", feature.AssigneeUserId);
            ViewData["FeatureCategoryId"] = new SelectList(_context.Set<FeatureCategory>(), "FeatureCategoryId", "Name", feature.FeatureCategoryId);
            ViewData["FeatureStatusId"] = new SelectList(_context.Set<FeatureStatus>(), "FeatureStatusId", "Name", feature.FeatureStatusId);
            ViewData["LastUpdateUserId"] = new SelectList(_context.Set<FeatureTrackerUser>(), "FeatureTrackerUserId", "FirstName", feature.LastUpdateUserId);
            ViewData["ReporterUserId"] = new SelectList(_context.Set<FeatureTrackerUser>(), "FeatureTrackerUserId", "FirstName", feature.ReporterUserId);

            return View(feature);
        }

        // POST: Features/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("FeatureId,ReporterUserId,AssigneeUserId,Title,Description,FeatureCategoryId,FeatureStatusId,CreationDate,UpdatedDate,LastUpdateUserId,Deleted")] Feature feature)
        {
            return await EditUtility(id, feature, false);
        }

        // GET: Features/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var feature = await _context.Feature
                .Include(f => f.AssigneeUser)
                .Include(f => f.FeatureCategory)
                .Include(f => f.FeatureStatus)
                .Include(f => f.LastUpdateUser)
                .Include(f => f.ReporterUser)
                .FirstOrDefaultAsync(m => m.FeatureId == id);
            if (feature == null)
            {
                return NotFound();
            }

            return View(feature);
        }

        // POST: Features/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id, [Bind("FeatureId,ReporterUserId,AssigneeUserId,Title,Description,FeatureCategoryId,FeatureStatusId,CreationDate,UpdatedDate,LastUpdateUserId,Deleted")] Feature feature)
        {
            return await EditUtility(id, feature, true);
        }

        private bool FeatureExists(int id)
        {
            return _context.Feature.Any(e => e.FeatureId == id);
        }

        private async Task<IActionResult> EditUtility(int id, [Bind("FeatureId,ReporterUserId,AssigneeUserId,Title,Description,FeatureCategoryId,FeatureStatusId,CreationDate,UpdatedDate,LastUpdateUserId,Deleted")] Feature feature, bool deleting)
        {
            if (id != feature.FeatureId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (deleting)
                        feature.Deleted = true;

                    feature.LastUpdateUserId = 1; // TODO change to current user
                    feature.UpdatedDate = DateTime.Now;

                    _context.Update(feature);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FeatureExists(feature.FeatureId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AssigneeUserId"] = new SelectList(_context.Set<FeatureTrackerUser>(), "FeatureTrackerUserId", "FirstName", feature.AssigneeUserId);
            ViewData["FeatureCategoryId"] = new SelectList(_context.Set<FeatureCategory>(), "FeatureCategoryId", "Name", feature.FeatureCategoryId);
            ViewData["FeatureStatusId"] = new SelectList(_context.Set<FeatureStatus>(), "FeatureStatusId", "Name", feature.FeatureStatusId);
            ViewData["LastUpdateUserId"] = new SelectList(_context.Set<FeatureTrackerUser>(), "FeatureTrackerUserId", "FirstName", feature.LastUpdateUserId);
            ViewData["ReporterUserId"] = new SelectList(_context.Set<FeatureTrackerUser>(), "FeatureTrackerUserId", "FirstName", feature.ReporterUserId);
            return View(feature);
        }
    }
}
