﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FeatureTracker.Models;

namespace FeatureTracker.Controllers
{
    public class FeatureCategoriesController : Controller
    {
        private readonly FeatureTrackerContext _context;

        public FeatureCategoriesController(FeatureTrackerContext context)
        {
            _context = context;
        }

        // GET: Configuration/FeatureCategories
        [HttpGet("Configuration/FeatureCategories")]
        public async Task<IActionResult> Index()
        {
            var featureTrackerContext = _context.FeatureCategory
                                            .Include(f => f.LastUpdateUser)
                                            .Where(f => f.Deleted != true);

            return View(await featureTrackerContext.ToListAsync());
        }

        // GET: Configuration/FeatureCategories/Create
        [HttpGet("Configuration/FeatureCategories/Create")]
        public IActionResult Create()
        {
            ViewData["LastUpdateUserId"] = new SelectList(_context.Set<FeatureTrackerUser>(), "FeatureTrackerUserId", "FirstName");
            return View();
        }

        // POST: FeatureCategories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost("Configuration/FeatureCategories/Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FeatureCategoryId,Name,LastUpdateUserId,Deleted")] FeatureCategory featureCategory)
        {
            if (ModelState.IsValid)
            {
                featureCategory.LastUpdateUserId = 1; // TODO change to current user
                featureCategory.Deleted = false;

                _context.Add(featureCategory);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["LastUpdateUserId"] = new SelectList(_context.Set<FeatureTrackerUser>(), "FeatureTrackerUserId", "FirstName", featureCategory.LastUpdateUserId);
            return View(featureCategory);
        }

        // GET: Configuration/FeatureCategories/Edit/5
        [HttpGet("Configuration/FeatureCategories/Edit/{id?}")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var featureCategory = await _context.FeatureCategory.FindAsync(id);
            if (featureCategory == null)
            {
                return NotFound();
            }
            ViewData["LastUpdateUserId"] = new SelectList(_context.Set<FeatureTrackerUser>(), "FeatureTrackerUserId", "FirstName", featureCategory.LastUpdateUserId);
            return View(featureCategory);
        }

        // POST: Configuration/FeatureCategories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost("Configuration/FeatureCategories/Edit/{id?}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("FeatureCategoryId,Name,LastUpdateUserId,Deleted")] FeatureCategory featureCategory)
        {
            return await EditUtility(id, featureCategory, false);
        }

        // GET: Configuration/FeatureCategories/Delete/5
        [HttpGet("Configuration/FeatureCategories/Delete/{id?}")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var featureCategory = await _context.FeatureCategory
                .Include(f => f.LastUpdateUser)
                .FirstOrDefaultAsync(m => m.FeatureCategoryId == id);
            if (featureCategory == null)
            {
                return NotFound();
            }

            return View(featureCategory);
        }

        // POST: FeatureCategories/Delete/5
        [HttpPost("Configuration/FeatureCategories/Delete/{id?}"), ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id, [Bind("FeatureCategoryId,Name,LastUpdateUserId,Deleted")] FeatureCategory featureCategory)
        {
            return await EditUtility(id, featureCategory, true);
        }

        private bool FeatureCategoryExists(int id)
        {
            return _context.FeatureCategory.Any(e => e.FeatureCategoryId == id);
        }

        private async Task<IActionResult> EditUtility(int id, [Bind("FeatureCategoryId,Name,LastUpdateUserId,Deleted")] FeatureCategory featureCategory, bool deleting)
        {
            if (id != featureCategory.FeatureCategoryId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (deleting)
                        featureCategory.Deleted = true;

                    featureCategory.LastUpdateUserId = 1; // TODO Change to current user

                    _context.Update(featureCategory);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FeatureCategoryExists(featureCategory.FeatureCategoryId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["LastUpdateUserId"] = new SelectList(_context.Set<FeatureTrackerUser>(), "FeatureTrackerUserId", "FirstName", featureCategory.LastUpdateUserId);
            return View(featureCategory);
        }
    }
}
