﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FeatureTracker.Models;
using System;

namespace FeatureTracker.Controllers
{
    public class FeatureTrackerUsersController : Controller
    {
        private readonly FeatureTrackerContext _context;

        public FeatureTrackerUsersController(FeatureTrackerContext context)
        {
            _context = context;
        }

        // GET: Configuration/Users
        [HttpGet("Configuration/Users")]
        public async Task<IActionResult> Index()
        {
            var featureTrackerContext = _context.FeatureTrackerUser
                                            .Include(f => f.LastUpdateUser)
                                            .Where(f => f.Deleted != true);

            return View(await featureTrackerContext.ToListAsync());
        }

        // GET: Configuration/Users/Details/5
        [HttpGet("Configuration/Users/Details/{id?}")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var featureTrackerUser = await _context.FeatureTrackerUser
                .Include(f => f.LastUpdateUser)
                .FirstOrDefaultAsync(m => m.FeatureTrackerUserId == id);
            if (featureTrackerUser == null)
            {
                return NotFound();
            }

            return View(featureTrackerUser);
        }

        // GET: Configuration/Users/Create
        [HttpGet("Configuration/Users/Create")]
        public IActionResult Create()
        {
            ViewData["LastUpdateUserId"] = new SelectList(_context.FeatureTrackerUser, "FeatureTrackerUserId", "FirstName");
            return View();
        }

        // POST: Configuration/Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost("Configuration/Users/Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FeatureTrackerUserId,FirstName,LastName,Email,CreationDate,UpdatedDate,LastUpdateUserId,Deleted")] FeatureTrackerUser featureTrackerUser)
        {
            if (ModelState.IsValid)
            {
                featureTrackerUser.LastUpdateUserId = 1; // TODO change to current user
                featureTrackerUser.CreationDate = DateTime.Now;
                featureTrackerUser.UpdatedDate = DateTime.Now;
                featureTrackerUser.Deleted = false;

                _context.Add(featureTrackerUser);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["LastUpdateUserId"] = new SelectList(_context.FeatureTrackerUser, "FeatureTrackerUserId", "FirstName", featureTrackerUser.LastUpdateUserId);
            return View(featureTrackerUser);
        }

        // GET: Configuration/Users/Edit/5
        [HttpGet("Configuration/Users/Edit/{id?}")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var featureTrackerUser = await _context.FeatureTrackerUser.FindAsync(id);
            if (featureTrackerUser == null)
            {
                return NotFound();
            }
            ViewData["LastUpdateUserId"] = new SelectList(_context.FeatureTrackerUser, "FeatureTrackerUserId", "FirstName", featureTrackerUser.LastUpdateUserId);
            return View(featureTrackerUser);
        }

        // POST: Configuration/Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost("Configuration/Users/Edit/{id?}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("FeatureTrackerUserId,FirstName,LastName,Email,CreationDate,UpdatedDate,LastUpdateUserId,Deleted")] FeatureTrackerUser featureTrackerUser)
        {
            return await EditUtility(id, featureTrackerUser, false);
        }

        // GET: Configuration/Users/Delete/5
        [HttpGet("Configuration/Users/Delete/{id?}")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var featureTrackerUser = await _context.FeatureTrackerUser
                .Include(f => f.LastUpdateUser)
                .FirstOrDefaultAsync(m => m.FeatureTrackerUserId == id);
            if (featureTrackerUser == null)
            {
                return NotFound();
            }

            return View(featureTrackerUser);
        }

        // POST: Configuration/Users/Delete/5
        [HttpPost("Configuration/Users/Delete/{id?}"), ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id, [Bind("FeatureTrackerUserId,FirstName,LastName,Email,CreationDate,UpdatedDate,LastUpdateUserId,Deleted")] FeatureTrackerUser featureTrackerUser)
        {
            return await EditUtility(id, featureTrackerUser, true);
        }

        private bool FeatureTrackerUserExists(int id)
        {
            return _context.FeatureTrackerUser.Any(e => e.FeatureTrackerUserId == id);
        }

        public async Task<IActionResult> EditUtility(int id, [Bind("FeatureTrackerUserId,FirstName,LastName,Email,CreationDate,UpdatedDate,LastUpdateUserId,Deleted")] FeatureTrackerUser featureTrackerUser, bool deleting)
        {
            if (id != featureTrackerUser.FeatureTrackerUserId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (deleting)
                        featureTrackerUser.Deleted = true;

                    featureTrackerUser.LastUpdateUserId = 1; // TODO change to current user;
                    featureTrackerUser.UpdatedDate = DateTime.Now;

                    _context.Update(featureTrackerUser);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FeatureTrackerUserExists(featureTrackerUser.FeatureTrackerUserId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["LastUpdateUserId"] = new SelectList(_context.FeatureTrackerUser, "FeatureTrackerUserId", "FirstName", featureTrackerUser.LastUpdateUserId);
            return View(featureTrackerUser);
        }
    }
}
