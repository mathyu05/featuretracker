﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FeatureTracker.Models;

namespace FeatureTracker.Controllers
{
    public class UpdateLogsController : Controller
    {
        private readonly FeatureTrackerContext _context;

        public UpdateLogsController(FeatureTrackerContext context)
        {
            _context = context;
        }

        // GET: Configuration/UpdateLogs
        [HttpGet("Configuration/UpdateLogs")]
        public async Task<IActionResult> Index()
        {
            var featureTrackerContext = _context.UpdateLog.Include(u => u.Action).Include(u => u.UpdatingUser);
            return View(await featureTrackerContext.ToListAsync());
        }
    }
}
